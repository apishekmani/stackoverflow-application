package com.example.demo.service;

import com.example.demo.adviceController.NoDataFoundException;
import com.example.demo.adviceController.ResourceNotFoundException;
import com.example.demo.converter.Converter;
import com.example.demo.dto.UserAllDTO;
import com.example.demo.dto.UserDTO;
import com.example.demo.dto.UserDisplayDTO;
import com.example.demo.repository.UserRepository;
import com.example.demo.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
@Service

public class UserService {
    @Autowired
    private UserRepository userRepository;

    public ResponseEntity<List<UserDisplayDTO>> getUsers() {
        List<UserDisplayDTO>userDisplayDTOList= userRepository.findAll().stream()
                .map(Converter::UserToUserDisplayDTO).collect(Collectors.toList());
        return new ResponseEntity<>(userDisplayDTOList, HttpStatus.OK);
    }
    public ResponseEntity<List<UserAllDTO>> getUsersAll() {
        List<UserAllDTO>userAllDTOList= userRepository.findAll().stream()
                .map(Converter::UserToUserAllDTO).collect(Collectors.toList());
        return new ResponseEntity<>(userAllDTOList, HttpStatus.OK);
    }

    public ResponseEntity<UserDisplayDTO>getByUserId(UUID userId){
        User user=userRepository.findById(userId).orElseThrow(() ->new ResourceNotFoundException("user not found for the id "+userId));
        return new ResponseEntity<>(Converter.UserToUserDisplayDTO(user),HttpStatus.OK);
    }

    public void addUser(UserDTO userDTO) throws NoSuchAlgorithmException {
        User user=new User();
        userRepository.save(Converter.UserDTOToUser(user,userDTO));
    }

    public void updateUser(UserDisplayDTO userDisplayDTO, UUID userId){
        User userToUpdate=userRepository.findById(userId).orElseThrow(() ->new ResourceNotFoundException("user Details is not found for the id "+userId));
        userRepository.save(Converter.UserDisplayDTOToUser(userToUpdate,userDisplayDTO));

    }

    public void deleteUser(UUID userId){
        User userToDelete=userRepository.findById(userId).orElseThrow(() ->new NoDataFoundException("user Details is not found for the id "+userId));
        userRepository.delete(userToDelete);

    }

}
