package com.example.demo.service;

import com.example.demo.adviceController.ResourceNotFoundException;
import com.example.demo.converter.Converter;
import com.example.demo.dto.QuestionAnswerDTO;
import com.example.demo.dto.QuestionDTO;
import com.example.demo.dto.QuestionDisplayDTO;
import com.example.demo.entity.Question;
import com.example.demo.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class QuestionService {
    @Autowired
    private QuestionRepository questionRepository;

    public ResponseEntity<List<QuestionDisplayDTO>> getQuestions(int offSet, int pageSize, String field) {
        List<QuestionDisplayDTO>questionDisplayDTOList= questionRepository.findAll(PageRequest.of(offSet,pageSize).withSort(Sort.by(field))).stream()
                .map(Converter::QuestionToQuestionDisplayDTO).collect(Collectors.toList());
        return new ResponseEntity<>(questionDisplayDTOList, HttpStatus.OK);
    }

    public ResponseEntity<QuestionDTO> getByQuestionId(UUID questionId) {
        Question question=questionRepository.findById(questionId).orElseThrow(() ->new ResourceNotFoundException("question not found for the id "+questionId));
        return new ResponseEntity<>(Converter.QuestionToQuestionDTO(question),HttpStatus.OK);
    }

    public ResponseEntity<QuestionAnswerDTO> getByQuestionAnswerId(UUID questionId) {
        Question question=questionRepository.findById(questionId).orElseThrow(() ->new ResourceNotFoundException("question not found for the id "+questionId));
        return new ResponseEntity<>(Converter.QuestionToQuestionAnswerDTO(question),HttpStatus.OK);
    }

    public void addQuestion(QuestionDTO questionDTO){
        Question question=new Question();
        questionRepository.save(Converter.QuestionDTOToQuestion(question,questionDTO));
    }

    public void updateQuestion(QuestionDTO questionDTO,UUID questionId){
        Question updateQuestion=questionRepository.findById(questionId).orElseThrow(() ->new ResourceNotFoundException("questions not found for the id "+questionId));
        questionRepository.save(Converter.QuestionDTOToQuestion(updateQuestion,questionDTO));
    }

    public void deleteQuestion(UUID questionId){
        Question deleteQuestion=questionRepository.findById(questionId).orElseThrow(() ->new ResourceNotFoundException("questions not found for the id "+questionId));
        questionRepository.delete(deleteQuestion);
    }

}
