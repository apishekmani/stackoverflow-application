package com.example.demo.service;

import com.example.demo.adviceController.ResourceNotFoundException;
import com.example.demo.converter.Converter;
import com.example.demo.dto.AnswersDTO;
import com.example.demo.dto.AnswersDisplayDTO;
import com.example.demo.dto.VotingDTO;
import com.example.demo.entity.Answers;
import com.example.demo.repository.AnswerRepository;
import com.example.demo.repository.QuestionRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AnswerService {
    @Autowired
    private AnswerRepository answerRepository;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private UserRepository userRepository;

    public ResponseEntity<List<AnswersDisplayDTO>> getAnswers(int offSet, int pageSize, String field) {
        List<AnswersDisplayDTO> answersDisplayDTOList = answerRepository.findAll(PageRequest.of(offSet,pageSize).withSort(Sort.by(field))).stream()
                .map(Converter::AnswersToAnswersDisplayDTO).collect(Collectors.toList());
        return new ResponseEntity<>(answersDisplayDTOList, HttpStatus.OK);
    }
    public ResponseEntity<AnswersDTO> getByAnswerId(UUID answerId) {
        Answers answers=answerRepository.findById(answerId).orElseThrow(() ->new ResourceNotFoundException("question not found for the id "+answerId));
        return new ResponseEntity<>(Converter.AnswersToAnswersDTO(answers),HttpStatus.OK);
    }

    public void addAnswers(@PathVariable UUID questionId, @RequestBody AnswersDTO answerDTO){
        Answers answers=new Answers();
        answerRepository.save(Converter.AnswersDTOToAnswers(answers,questionId,answerDTO,questionRepository));
    }
    public void addVoting(@PathVariable UUID answerId,@RequestBody VotingDTO votingDTO){
        Answers answers=new Answers();
        answerRepository.save(Converter.VotingDTOToAnswers(answers,answerId,votingDTO,answerRepository,userRepository));
    }

    public void updateAnswer(AnswersDTO answersDTO, UUID answerId){
        Answers updateAnswers=answerRepository.findById(answerId).orElseThrow(() ->new ResourceNotFoundException("answers not found for the id "+answerId));
        answerRepository.save(Converter.updateAnswersDTOToAnswers(updateAnswers,answersDTO));
    }

    public void deleteAnswer(UUID answerId){
        Answers deleteAnswer=answerRepository.findById(answerId).orElseThrow(() ->new ResourceNotFoundException("answers not found for the id "+answerId));
        answerRepository.delete(deleteAnswer);
    }

}
