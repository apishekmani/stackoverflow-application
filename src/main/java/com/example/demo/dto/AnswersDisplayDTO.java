package com.example.demo.dto;

import java.util.UUID;

public class AnswersDisplayDTO {
    private String userName;
    private UUID answerId;
    private String answer;
    private long upVote;
    private long downVote;
    public AnswersDisplayDTO(){

    }

    public AnswersDisplayDTO(String userName,UUID answerId, String answer,long upVote,long downVote) {
        this.userName = userName;
        this.answerId=answerId;
        this.answer = answer;
        this.upVote=upVote;
        this.downVote=downVote;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public UUID getAnswerId() {
        return answerId;
    }

    public void setAnswerId(UUID answerId) {
        this.answerId = answerId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public long getUpVote() {
        return upVote;
    }

    public void setUpVote(long upVote) {
        this.upVote = upVote;
    }

    public long getDownVote() {
        return downVote;
    }

    public void setDownVote(long downVote) {
        this.downVote = downVote;
    }
}
