package com.example.demo.dto;


public class VotingDTO {
    private String userName;
    private String vote;
    public VotingDTO(){

    }

    public VotingDTO(String userName,String vote) {
        this.userName=userName;
        this.vote = vote;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getVote() {
        return vote;
    }

    public void setVote(String vote) {
        this.vote = vote;
    }

}
