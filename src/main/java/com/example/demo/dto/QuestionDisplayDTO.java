package com.example.demo.dto;

import com.example.demo.entity.Answers;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class QuestionDisplayDTO {
    private String userName;
    private UUID questionId;
    private String question;
    private List<Answers> answers;
    public QuestionDisplayDTO(){
        this.answers=new ArrayList<>();

    }

    public QuestionDisplayDTO(String userName,UUID questionId, String question,List<Answers> answers) {
        this.userName = userName;
        this.questionId = questionId;
        this.question = question;
        this.answers=answers;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public UUID getQuestionId() {
        return questionId;
    }

    public void setQuestionId(UUID questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<Answers> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answers> answers) {
        this.answers = answers;
    }
}
