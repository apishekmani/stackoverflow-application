package com.example.demo.dto;

import com.example.demo.entity.Answers;

import java.util.ArrayList;
import java.util.List;

public class QuestionAnswerDTO {
    private String userName;
    private String question;
    private List<Answers> answers;
    public QuestionAnswerDTO(){
        this.answers=new ArrayList<>();
    }

    public QuestionAnswerDTO(String userName, String question, List<Answers> answers) {
        this.userName = userName;
        this.question = question;
        this.answers = answers;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<Answers> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answers> answers) {
        this.answers = answers;
    }

}
