package com.example.demo.dto;


public class AnswersDTO {
    private String userName;
    private String answer;
    public AnswersDTO(){

    }

    public AnswersDTO(String userName, String answer) {
        this.userName = userName;
        this.answer = answer;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
