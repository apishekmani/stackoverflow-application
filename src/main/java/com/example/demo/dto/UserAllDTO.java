package com.example.demo.dto;


public class UserAllDTO {
    private String userName;
    private String mobileNumber;
    private long points;
    public UserAllDTO(){

    }

    public UserAllDTO(String userName, String mobileNumber, long points) {
        this.userName = userName;
        this.mobileNumber = mobileNumber;
        this.points=points;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }

}
