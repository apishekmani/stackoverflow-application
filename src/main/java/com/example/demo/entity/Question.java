package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name="Question")
public class Question {
    @Column(name="userName")
    private String userName;
    @Column(name="id")
    @Id
    @GeneratedValue
    private UUID questionId;
    @Column(name="question")
    private String question;
    @Column(name="CreationTime")
    @CreationTimestamp
    private Date questionCreationTime;

    @JsonManagedReference
    @Column(name="AnswerId")
    @OneToMany(mappedBy="question",cascade=CascadeType.ALL)
    private List<Answers> answers;
    public Question(){
        this.answers=new ArrayList<>();
    }

    public Question(String userName,UUID questionId, String question, Date questionCreationTime, List<Answers> answers) {
        this.userName = userName;
        this.questionId = questionId;
        this.question = question;
        this.questionCreationTime = questionCreationTime;
        this.answers = answers;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public UUID getQuestionId() {
        return questionId;
    }

    public void setQuestionId(UUID questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Date getQuestionCreationTime() {
        return questionCreationTime;
    }

    public void setQuestionCreationTime(Date questionCreationTime) {
        this.questionCreationTime = questionCreationTime;
    }

    public List<Answers> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answers> answers) {
        this.answers = answers;
    }
}
