package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.UUID;


@Entity
@Table(name="Answers")
public class Answers {
    @Column(name = "userName")
    private String userName;
    @Column(name="AnswerId")
    @Id
    @GeneratedValue
    private UUID answerId;
    @Column(name = "Answer")
    private String answer;
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="questionId")
    private Question question;
    private long upVote;
    private long downVote;
    public Answers() {

    }
    public Answers(String userName, UUID answerId,String answer,Question question,long upVote,long downVote) {
        this.userName = userName;
        this.answerId = answerId;
        this.answer = answer;
        this.question=question;
        this.upVote=upVote;
        this.downVote=downVote;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public UUID getAnswerId() {
        return answerId;
    }

    public void setAnswerId(UUID answerId) {
        this.answerId = answerId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public long getUpVote() {
        return upVote;
    }

    public void setUpVote(long upVote) {
        this.upVote = upVote;
    }

    public long getDownVote() {
        return downVote;
    }

    public void setDownVote(long downVote) {
        this.downVote = downVote;
    }
}
