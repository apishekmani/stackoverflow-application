package com.example.demo.controller;

import com.example.demo.dto.AnswersDTO;
import com.example.demo.dto.AnswersDisplayDTO;
import com.example.demo.dto.VotingDTO;
import com.example.demo.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/v1/stackOverFlow")
public class AnswerController {
    @Autowired
    private AnswerService answerService;

    @GetMapping("/answers/{offSet}/{pageSize}/{field}")
    public ResponseEntity<List<AnswersDisplayDTO>>getAnswers(@PathVariable int offSet, @PathVariable int pageSize, @PathVariable String field){
        return answerService.getAnswers(offSet,pageSize,field);
    }

    @GetMapping("/answers/{answersId}")
    public ResponseEntity<AnswersDTO>getByAnswersId(@PathVariable UUID answersId)
    {
        return answerService.getByAnswerId(answersId);
    }

    @PostMapping("/{questionId}/answers")
    public ResponseEntity<HttpStatus>addAnswers(@PathVariable UUID questionId,@RequestBody AnswersDTO answersDTO){
        answerService.addAnswers(questionId,answersDTO);
        return ResponseEntity.ok().body(HttpStatus.ACCEPTED);
    }

    @PostMapping("/answers/{answersId}/voting")
    public ResponseEntity<HttpStatus>getByAnswersId(@PathVariable UUID answersId, @RequestBody VotingDTO votingDTO)
    {
        answerService.addVoting(answersId,votingDTO);
        return ResponseEntity.ok().body(HttpStatus.OK);
    }

    @PutMapping("/answers/{answerId}")
    public ResponseEntity<HttpStatus>updateAnswers(@RequestBody AnswersDTO answersDTO, @PathVariable UUID answerId){
        answerService.updateAnswer(answersDTO,answerId);
        return ResponseEntity.ok().body(HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/answers/{answerId}")
    public ResponseEntity<HttpStatus>deleteAnswer(@PathVariable UUID answerId){
        answerService.deleteAnswer(answerId);
        return ResponseEntity.ok().body(HttpStatus.OK);
    }

}
