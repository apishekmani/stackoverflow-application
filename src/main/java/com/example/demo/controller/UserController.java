package com.example.demo.controller;

import com.example.demo.dto.UserAllDTO;
import com.example.demo.dto.UserDTO;
import com.example.demo.dto.UserDisplayDTO;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;


import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/v1/stackOverFlow")
public class UserController {
    @Autowired
    private UserService userService;
    @GetMapping("/users")
    public ResponseEntity<List<UserDisplayDTO>>getUsers(){
        return userService.getUsers();
    }
    @GetMapping("/usersAll")
    public ResponseEntity<List<UserAllDTO>>getUsersAll(){
        return userService.getUsersAll();
    }

    @GetMapping("/users/{userId}")
    public ResponseEntity<UserDisplayDTO>getUsersById(@PathVariable UUID userId){
        return userService.getByUserId(userId);
    }

    @PostMapping("/users")
    public ResponseEntity<HttpStatus>addUser(@RequestBody UserDTO userDTO) throws NoSuchAlgorithmException {
        userService.addUser(userDTO);
        return ResponseEntity.ok().body(HttpStatus.CREATED);
    }

    @PutMapping("/users/{userId}")
    public ResponseEntity<HttpStatus>updateUser(@RequestBody UserDisplayDTO userDisplayDTO, @PathVariable UUID userId){
        userService.updateUser(userDisplayDTO,userId);
        return ResponseEntity.ok().body(HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/users/{userId}")
    public ResponseEntity<HttpStatus>deleteUser(@PathVariable UUID userId){
        userService.deleteUser(userId);
        return ResponseEntity.ok().body(HttpStatus.OK);
    }

}
