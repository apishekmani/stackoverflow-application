package com.example.demo.controller;

import com.example.demo.dto.QuestionAnswerDTO;
import com.example.demo.dto.QuestionDTO;
import com.example.demo.dto.QuestionDisplayDTO;
import com.example.demo.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;


import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/v1/stackOverFlow")
public class QuestionController {
    @Autowired
    private QuestionService questionService;

    @GetMapping("/questions/{offSet}/{pageSize}/{field}")
    public ResponseEntity<List<QuestionDisplayDTO>> getQuestions(@PathVariable int offSet, @PathVariable int pageSize, @PathVariable String field){
        return questionService.getQuestions(offSet,pageSize,field);
    }

    @GetMapping("/questions/{questionId}")
    public ResponseEntity<QuestionDTO>getQuestionsById(@PathVariable UUID questionId){
        return questionService.getByQuestionId(questionId);
    }

    @GetMapping("/questionAnswers/{questionId}")
    public ResponseEntity<QuestionAnswerDTO>getQuestionsAnswerById(@PathVariable UUID questionId){
        return questionService.getByQuestionAnswerId(questionId);
    }

    @PostMapping("/questions")
    public ResponseEntity<HttpStatus>addQuestion(@RequestBody QuestionDTO questionDTO){
        questionService.addQuestion(questionDTO);
        return ResponseEntity.ok().body(HttpStatus.CREATED);
    }

    @PutMapping("/questions/{questionId}")
    public ResponseEntity<HttpStatus>updateQuestions(@RequestBody QuestionDTO questionDTO,@PathVariable UUID questionId){
        questionService.updateQuestion(questionDTO,questionId);
        return ResponseEntity.ok().body(HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/questions/{questionId}")
    public ResponseEntity<HttpStatus>deleteQuestion(@PathVariable UUID questionId){
        questionService.deleteQuestion(questionId);
        return ResponseEntity.ok().body(HttpStatus.OK);
    }

}
