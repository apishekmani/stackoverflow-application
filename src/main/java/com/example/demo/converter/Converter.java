package com.example.demo.converter;

import com.example.demo.adviceController.ResourceNotFoundException;
import com.example.demo.dto.*;
import com.example.demo.entity.Answers;
import com.example.demo.entity.Question;
import com.example.demo.repository.AnswerRepository;
import com.example.demo.repository.QuestionRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.user.User;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.UUID;


public class Converter {

    public static UserDisplayDTO UserToUserDisplayDTO(User user){
        UserDisplayDTO userDisplayDTO=new UserDisplayDTO();
        userDisplayDTO.setUserName(user.getUserName());
        userDisplayDTO.setMobileNumber(user.getMobileNumber());
        return userDisplayDTO;
    }
    public static UserAllDTO UserToUserAllDTO(User user){
        UserAllDTO userAllDTO=new UserAllDTO();
        userAllDTO.setUserName(user.getUserName());
        userAllDTO.setMobileNumber(user.getMobileNumber());
        userAllDTO.setPoints(user.getPoints());
        return userAllDTO;
    }
    public static User UserDisplayDTOToUser(User user, UserDisplayDTO userDisplayDTO){
        user.setUserName(userDisplayDTO.getUserName());
        user.setMobileNumber(userDisplayDTO.getMobileNumber());
        return user;
    }

    public static User UserDTOToUser(User user, UserDTO userDTO) throws NoSuchAlgorithmException {
        user.setUserName(userDTO.getUserName());
        user.setPassword(hashing(userDTO.getPassword()));
        user.setMobileNumber(userDTO.getMobileNumber());
        return user;
    }

    public static QuestionDisplayDTO QuestionToQuestionDisplayDTO(Question question){
        QuestionDisplayDTO questionDisplayDTO=new QuestionDisplayDTO();
        questionDisplayDTO.setUserName(question.getUserName());
        questionDisplayDTO.setQuestionId(question.getQuestionId());
        questionDisplayDTO.setQuestion(question.getQuestion());
        questionDisplayDTO.setAnswers(question.getAnswers());
        return questionDisplayDTO;
    }

   public static QuestionDTO QuestionToQuestionDTO(Question question){
        QuestionDTO questionDTO=new QuestionDTO();
        questionDTO.setUserName(question.getUserName());
        questionDTO.setQuestion(question.getQuestion());
        return questionDTO;
   }

    public static QuestionAnswerDTO QuestionToQuestionAnswerDTO(Question question){
        QuestionAnswerDTO questionAnswerDTO=new QuestionAnswerDTO();
        questionAnswerDTO.setUserName(question.getUserName());
        questionAnswerDTO.setQuestion(question.getQuestion());
        questionAnswerDTO.setAnswers(question.getAnswers());
        return questionAnswerDTO;
    }

   public static Question QuestionDTOToQuestion(Question question,QuestionDTO questionDTO){
        question.setUserName(questionDTO.getUserName());
        question.setQuestion(questionDTO.getQuestion());
        return question;
   }

    public static AnswersDisplayDTO AnswersToAnswersDisplayDTO(Answers answers){
        AnswersDisplayDTO answersDisplayDTO=new AnswersDisplayDTO();
        answersDisplayDTO.setUserName(answers.getUserName());
        answersDisplayDTO.setAnswerId(answers.getAnswerId());
        answersDisplayDTO.setAnswer(answers.getAnswer());
        answersDisplayDTO.setUpVote(answers.getUpVote());
        answersDisplayDTO.setDownVote(answers.getDownVote());
        return answersDisplayDTO;
    }

    public static Answers VotingDTOToAnswers(Answers answers, UUID answerId, VotingDTO votingDTO, AnswerRepository answerRepository,UserRepository userRepository){
        answers=answerRepository.findById(answerId).orElseThrow(() ->new ResourceNotFoundException("Answer not found for the id "+answerId));
        User user= userRepository.findByUserName(votingDTO.getUserName());
        if(Objects.equals(votingDTO.getVote(), "upVote")){
            answers.setUpVote(answers.getUpVote()+1);
            user.setPoints(user.getPoints()+10);
        }
        else{
            answers.setDownVote(answers.getDownVote()+1);
            user.setPoints(user.getPoints()-5);
        }
        return answers;
    }

   public static AnswersDTO AnswersToAnswersDTO(Answers answers){
        AnswersDTO answersDTO=new AnswersDTO();
        answersDTO.setUserName(answers.getUserName());
        answersDTO.setAnswer(answers.getAnswer());
        return answersDTO;
   }

   public static Answers AnswersDTOToAnswers(Answers answers,UUID questionId,AnswersDTO answersDTO,QuestionRepository questionRepository){
        answers.setUserName(answersDTO.getUserName());
        answers.setAnswer(answersDTO.getAnswer());
        Question question=questionRepository.findById(questionId).orElseThrow(() -> new ResourceNotFoundException("No question found for the id "+questionId));
        answers.setQuestion((question));
        return answers;
   }

    public static Answers updateAnswersDTOToAnswers(Answers answers, AnswersDTO answersDTO) {
        answers.setUserName(answersDTO.getUserName());
        answers.setAnswer(answersDTO.getAnswer());
        return answers;
    }

    public static String hashing(String password) throws NoSuchAlgorithmException {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(password.getBytes());
            byte[] result = messageDigest.digest();
            StringBuilder s = new StringBuilder();
            for (byte b : result) {
                s.append(String.format("%02x", b));
            }
            return s.toString();
        }
            catch(NoSuchAlgorithmException e){
                e.printStackTrace();
            }
        throw new NoSuchAlgorithmException("No algorithm found");
    }

}
