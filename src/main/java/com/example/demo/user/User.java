package com.example.demo.user;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.UUID;

@Entity
public class User {
    private String userName;
    private String password;
    @Id
    @GeneratedValue
    private UUID userId;
    @Pattern(regexp="[0-9]{10}",message="your mobile number is wrong")
    private String mobileNumber;
    @CreationTimestamp
    private Date userCreationTime;
    private long points;
    
    public User(){

    }
    public User(String userName, String password, UUID userId, String mobileNumber, Date userCreationTime,long points) {
        this.userName = userName;
        this.password = password;
        this.userId = userId;
        this.mobileNumber = mobileNumber;
        this.userCreationTime = userCreationTime;
        this.points=points;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Date getUserCreationTime() {
        return userCreationTime;
    }

    public void setUserCreationTime(Date userCreationTime) {
        this.userCreationTime = userCreationTime;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }
}
