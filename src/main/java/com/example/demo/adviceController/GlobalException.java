package com.example.demo.adviceController;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalException {

    @ExceptionHandler
    public ResponseEntity<Error> handleResourceNotFoundException(ResourceNotFoundException e){
        Error error=new Error();
        error.setStatusCode(HttpStatus.NOT_FOUND.value());
        error.setMessage(e.getMessage());
        return new ResponseEntity<Error>(error,HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<Error> handleNoDataFoundException(NoDataFoundException e){
        Error error=new Error();
        error.setStatusCode(HttpStatus.NO_CONTENT.value());
        error.setMessage(e.getMessage());
        return new ResponseEntity<Error>(error,HttpStatus.OK);
    }

}
