package com.example.demo.adviceController;

public class NoDataFoundException extends RuntimeException{
        public NoDataFoundException(String message){
            super(message);
        }
    }
